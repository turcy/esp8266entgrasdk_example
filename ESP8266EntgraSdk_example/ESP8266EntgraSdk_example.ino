// #################################################
// Copyright (C) Entgra (Pvt) Ltd, Inc - All Rights Reserved.
// Unauthorised copying/redistribution of this file, via any medium is strictly prohibited
// Proprietary and confidential.
//
// Licensed under the Entgra Commercial License, Version 1.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// https://entgra.io/licenses/entgra-commercial/1.0
// #################################################

#include <EntgraSdk.h>
#include <ESP8266WiFi.h>
#include "DHT.h"

#define DHT_PIN 2
#define BUZZER_PIN 15
#define LED_PIN 16

#define DHT_TYPE DHT11

const char* ssid = "";//"your wifi ssid";
const char* password = "";//"your wifi password";
const char* host = "";//Server IP

// #################################################
// Credentials for initCredentials() method.
const char* userName = "admin";
const char* passWord = "admin";

const char* deviceId = "dk001";
const char* deviceType = "demo_kit";
const char* tenantDomain = "carbon.super";
// #################################################

long currentMillis = 0;

DHT dht(DHT_PIN, DHT_TYPE);

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

// Create sdk_lib instance
EntgraSdk SDK(host, onOperation);

void onOperation(String id, String code, String payload) {
  //handle arrived messages
  Serial.print("Operation arrived [");
  Serial.print(code);
  Serial.print("] ");
  Serial.println();
  Serial.println(payload);
  Serial.println();
  
  if(code == "buzzer"){
      if(payload == "on"){
        tone(BUZZER_PIN, 1000);
      }else if(payload == "off"){
        noTone(BUZZER_PIN);
      }
  }else if(code == "led"){
      if(payload == "on"){
        Serial.println("LED ON");
        digitalWrite(LED_PIN, HIGH);
      }else if(payload == "off"){
        Serial.println("LED OFF");
        digitalWrite(LED_PIN, LOW);
      }
  }

  String operationStatus = "COMPLETED"; 
  String operationResponse = "this is my response";
  //After an operation following method is called to send the operation response.
  SDK.operationResponse((char*)id.c_str(), (char*)operationStatus.c_str(), (char*)operationResponse.c_str());
}

// ####################################################
// Uncomment this for initConfig() method.
/*
  Property prop1 = {"firmware_version", "1.0"};
  Property prop2 = {"chipid", String(ESP.getChipId())};

  Property* propertyList[3] = {&prop1, &prop2};
  int numberOfProps = 2;
  String token = String(ESP.getChipId());
*/
// ####################################################

void setup() {
  pinMode(BUZZER_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT); 
  digitalWrite(LED_PIN, LOW);
  dht.begin();

  
  Serial.begin(115200);
  setup_wifi();
  currentMillis = millis();

  int i = 0;
  int period;
  while (true) {
    // Only one of following two functions should be called.
    if (SDK.initCredentials(userName, passWord, deviceId, deviceType, tenantDomain)) {
      break; // Initiate the client by giving credentials
    }
    //if(SDK.initConfig(token, propertyList, numberOfProps)){break;}// Initiate the client with /config endpoint credentials

    if (i <= 5) {
      period = 5 * pow(2, i);
    } else {
      period = 5 * pow(2, 6);
    }
    Serial.print("Unable to initiate. Retrying in ");
    Serial.print(period);
    Serial.println(" seconds.");
    delay(period * 1000);
    i++;
  }

  Serial.println( "###### Compiled :00006: " __DATE__ ", " __TIME__ " ######");
  
}

void loop() {
  // Continously keep the client alive
  SDK.loop();

  if (currentMillis + 10000 < millis()) {
      float h = dht.readHumidity();
      float t = dht.readTemperature();
      if (isnan(h) || isnan(t)) {
        Serial.println(F("Failed to read from DHT sensor!"));
      }
      Serial.print(F("Humidity: "));
      Serial.print(h);
      Serial.print(F("%  Temperature: "));
      Serial.print(t);
      Serial.println(F("°C "));
    String payload = "{\"millis\": "+ String(millis()) + ",\"humidity\":"+ String(h) +",\"temperature\": "+ String(t) + "}" ;
    SDK.publish(payload);
    Serial.println();
    currentMillis = millis();
  }
}
